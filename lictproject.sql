-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 25, 2017 at 03:22 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lictproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `acclogin`
--

CREATE TABLE `acclogin` (
  `id` int(120) NOT NULL,
  `accname` varchar(200) NOT NULL,
  `accpassword` varchar(220) NOT NULL,
  `accemail` varchar(256) NOT NULL,
  `accphonenumber` varchar(280) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acclogin`
--

INSERT INTO `acclogin` (`id`, `accname`, `accpassword`, `accemail`, `accphonenumber`) VALUES
(1, 'admin', 'admin', 'admin@gmail.com', '01767292882'),
(2, 'admin2', 'admin2', 'admin2@gmail.com', '01818159822');

-- --------------------------------------------------------

--
-- Table structure for table `addstudent`
--

CREATE TABLE `addstudent` (
  `id` int(200) NOT NULL,
  `stid` varchar(200) NOT NULL,
  `stname` varchar(220) NOT NULL,
  `stemail` varchar(255) NOT NULL,
  `staddress` varchar(250) NOT NULL,
  `stmobilenumber` varchar(220) NOT NULL,
  `stgender` varchar(80) NOT NULL,
  `stmaritialstatus` varchar(80) NOT NULL,
  `stcourse` varchar(80) NOT NULL,
  `streceivable` varchar(120) NOT NULL,
  `stpaid` varchar(120) NOT NULL,
  `stdue` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addstudent`
--

INSERT INTO `addstudent` (`id`, `stid`, `stname`, `stemail`, `staddress`, `stmobilenumber`, `stgender`, `stmaritialstatus`, `stcourse`, `streceivable`, `stpaid`, `stdue`) VALUES
(1, '143-35-779', 'Golam Rabbani', 'gr.rishad@gmail.com', 'Kolabagan', '01767292882', 'Male', 'Single', 'Java', '12000', '10000', '2000'),
(2, '131-35-465', 'yamin', 'yamin@gmail.com', 'kolabagan', '01751300314', 'male', 'single', 'Php', '15000', '10000', '5000');

-- --------------------------------------------------------

--
-- Table structure for table `adminlogin`
--

CREATE TABLE `adminlogin` (
  `id` int(100) NOT NULL,
  `name` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminlogin`
--

INSERT INTO `adminlogin` (`id`, `name`, `password`) VALUES
(1, 'admin', 'admin'),
(2, 'admin2', 'admin2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acclogin`
--
ALTER TABLE `acclogin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addstudent`
--
ALTER TABLE `addstudent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adminlogin`
--
ALTER TABLE `adminlogin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acclogin`
--
ALTER TABLE `acclogin`
  MODIFY `id` int(120) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `addstudent`
--
ALTER TABLE `addstudent`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `adminlogin`
--
ALTER TABLE `adminlogin`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
